package pkg1

import (
	"fmt"
)

func HelloWorldFromPkg1() {
	fmt.Printf("Hello world from pkg1!\n")
}

func main() {
	HelloWorldFromPkg1()
}
