package pkg2

import (
	"fmt"
)

func HelloWorldFromPkg2() {
	fmt.Printf("Hello world from pkg2!\n")
}

func main() {
	HelloWorldFromPkg2()
}
